"""
Created on Mon Sep 23 18:36:45 2019
@author: WQD170073
"""

import schedule 
import time
# from selenium import webdriver

while time.localtime().tm_hour != 20:
    time.sleep(1)
    print("Hi")
    
print("Bye")

# Task scheduling 
# After every 10mins geeks() is called.  
schedule.every(10).minutes.do(geeks) 
  
# After every hour geeks() is called. 
schedule.every().hour.do(geeks) 
  
# Every day at 12am or 00:00 time bedtime() is called. 
schedule.every().day.at("00:00").do(bedtime) 
  
# After every 5 to 10mins in between run work() 
schedule.every(5).to(10).minutes.do(work) 
  
# Every monday good_luck() is called 
schedule.every().monday.do(good_luck) 
  
# Every tuesday at 18:00 sudo_placement() is called 
schedule.every().tuesday.at("18:00").do(sudo_placement) 
  
# Loop so that the scheduling task 
# keeps on running all time. 
while True: 
  
    # Checks whether a scheduled task  
    # is pending to run or not 
    schedule.run_pending() 
    time.sleep(1) 

driver = webdriver.Chrome(r'C:\Users\drivers\chromedriver.exe')
driver.maximize_window()
driver.get("http://www.seleniumeasy.com/test/basic-first-form-demo.html")
assert "Selenium Easy Demo - Simple Form to Automate using Selenium" in driver.title

eleUserMessage = driver.find_element_by_id("user-message")
eleUserMessage.clear()
eleUserMessage.send_keys("Test Python")

eleShowMsgBtn=driver.find_element_by_css_selector('#get-input > .btn')
eleShowMsgBtn.click()

eleYourMsg=driver.find_element_by_id("display")
assert "Test Python" in eleYourMsg.text
driver.close()

# https://www.seleniumeasy.com/python/example-code-using-selenium-webdriver-python
# https://www.geeksforgeeks.org/python-schedule-library/
